﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BtnScript : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler
{
    public void OnPointerEnter(PointerEventData eventData) {
        GetComponentInChildren<Text>().color = new Color(0.937255f, 0.4980392f, 0.1019608f);
    }

    public void OnPointerExit(PointerEventData eventData) {
        GetComponentInChildren<Text>().color = new Color(0.1960784f, 0.1960784f, 0.1960784f);
    }
}
