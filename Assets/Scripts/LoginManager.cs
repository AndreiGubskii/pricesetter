﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginManager : MonoBehaviour {
    [SerializeField] private InputField loginField;
    [SerializeField] private InputField passwordField;
    [SerializeField] private Button submit;
    [SerializeField] private Text errorMessage;
    [SerializeField] private GameObject loadBackground;
    /// <summary>
    /// Логин
    /// </summary>
    private string Login {
        set => PlayerPrefs.SetString("login", value);
        get => PlayerPrefs.GetString("login");
    }
    /// <summary>
    /// Пароль
    /// </summary>
    private string Password {
        set => PlayerPrefs.SetString("password", value);
        get => PlayerPrefs.GetString("password");
    }

    private void Start() {
        loginField.text = Login;
        passwordField.text = Password;
        submit.onClick.AddListener(Submit);


    }
    /// <summary>
    /// Событие на нажате кнопки "Вход"
    /// </summary>
    private void Submit() {
        errorMessage.text = string.Empty;
        if (loginField.text != string.Empty && passwordField.text != string.Empty) {
            loadBackground.SetActive(true);
            StartCoroutine(LoginIEnumerator(loginField.text, passwordField.text));
        }
    }


    IEnumerator LoginIEnumerator(string login,string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("login", login);
        form.AddField("pass", password);

        UnityWebRequest www = UnityWebRequest.Post("http://proparts.kg/", form);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            errorMessage.text = www.error;
        }
        else {
            string cookie = www.GetResponseHeader("Set-Cookie");
            if (!string.IsNullOrEmpty(cookie) && cookie.Contains("PHPSESSID") && cookie.Contains("ABCPUser")) {
                DataManager.instance.Cookie = cookie;
                DataManager.instance.Login = loginField.text;
                Login = loginField.text;
                Password = passwordField.text;
                SceneManager.LoadScene("main");
            }
            else {
                Debug.Log("Неверный логин или пароль!");
                errorMessage.text = "Неверный логин или пароль!";
            }
        }
        loadBackground.SetActive(false);
    }
}
