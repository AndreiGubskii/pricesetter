﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Preloading : MonoBehaviour
{
    //Необходимо перед входом произвести logout чтобы очистить cookie
    IEnumerator Start() {
        UnityWebRequest www = UnityWebRequest.Get("http://proparts.kg/?logout");

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else {
            SceneManager.LoadScene("login");
        }
        
    }


}
