﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using SFB;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class main : MonoBehaviour
{
    public delegate void CallBack(string uri, string lastPage);
    /// <summary>
    /// Срок доставки (дней)
    /// </summary>
    private int Deadline
    {
        get => PlayerPrefs.GetInt("deadline", 7);
        set => PlayerPrefs.SetInt("deadline", value);
    }
    /// <summary>
    /// Задержка между запросами
    /// </summary>
    private int Delay
    {
        get => PlayerPrefs.GetInt("delay", 5);
        set => PlayerPrefs.SetInt("delay", value);
    }

    private string Path { get; set; }
    [SerializeField] private Button startBtn;
    [SerializeField] private Button selectFile;
    [SerializeField] private GameObject loadPanel;
    [SerializeField] private Text loadText;
    [SerializeField] private Text versionText;
    [SerializeField] private InputField input;
    [SerializeField] private InputField deadlineInput;
    [SerializeField] private InputField errorText;
    [SerializeField] private InputField delayInput;
    [SerializeField] private Button logout;
    [SerializeField] private Text loginText;

    /// <summary>
    /// Наименование сохраняемого файла
    /// </summary>
    private string FileName
    {
        get { return PlayerPrefs.GetString("fileName", "file-" + DateTime.Now.Ticks); }
        set { PlayerPrefs.SetString("fileName", value); }
    }

    void Start()
    {
        delayInput.text = Delay.ToString();
        loginText.text = DataManager.instance.Login;
        versionText.text = $"version: {Application.version}";
        startBtn.onClick.AddListener(StartClickListener);
        selectFile.onClick.AddListener(SelectFile);
        delayInput.onEndEdit.AddListener(SetDelay);
        deadlineInput.onEndEdit.AddListener(SetDeadline);
        deadlineInput.text = Deadline.ToString();
        logout.onClick.AddListener(Logout);
    }
    /// <summary>
    /// Событие. Срабатывает при установки сроков доставки
    /// </summary>
    private void SetDeadline(string arg0)
    {
        if (arg0.Contains("-"))
        {
            deadlineInput.text = arg0.Replace("-", "");
        }
        Deadline = Math.Abs(int.Parse(arg0));
    }

    /// <summary>
    /// Выход из учетной записи
    /// </summary>
    private void Logout()
    {
        StartCoroutine(LogoutIEnumerable());
    }

    private IEnumerator LogoutIEnumerable()
    {
        loadText.text = "Ожидайте...";
        loadPanel.SetActive(true);
        UnityWebRequest www = UnityWebRequest.Get("http://proparts.kg/?logout");

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            SetErrorMessage(www.error, Color.red);
        }
        else
        {
            SceneManager.LoadScene("login");
        }
        loadPanel.SetActive(false);
    }
    /// <summary>
    /// Событие. Срабатывает при установки задержки между запросам
    /// </summary>
    private void SetDelay(string arg0)
    {
        if (arg0.Contains("-"))
        {
            delayInput.text = arg0.Replace("-", "");
        }
        if (arg0 == string.Empty)
        {
            arg0 = "0";
            delayInput.text = arg0;
        }
        Delay = Math.Abs(int.Parse(arg0));
    }
    /// <summary>
    /// Выбор Файла CSV
    /// </summary>
    private void SelectFile()
    {
        Path = StandaloneFileBrowser.OpenFilePanel("Выберите файл", "", "csv", false)?.First();
        input.text = Path;
        SetErrorMessage("", Color.black);
    }
    /// <summary>
    /// Событие на нажатие кнопки "Старт"
    /// </summary>
    private void StartClickListener()
    {
        if (string.IsNullOrEmpty(Path))
        {
            SetErrorMessage("Вы не выбрали файл!", Color.red);
        }
        else if (string.IsNullOrEmpty(deadlineInput.text))
        {
            SetErrorMessage("Вы не установили сроки планируемой доставки!", Color.red);
        }
        else
        {
            StartCoroutine(Request(Deadline, Path));
        }
    }

    /// <summary>
    /// Запрос 
    /// </summary>
    private IEnumerator Request(int deadLine, string path)
    {
        List<string> result = new List<string>();
        result.Add("Артикул;Склад;Наличие;Срок;Цена;Подробности");

        string brands = "DYNAMATRIXKOREA";
        string pbrandName = "DYNAMATRIX-KOREA";
        List<string> articles = File.ReadAllLines(path).ToList();
        loadPanel.SetActive(true);

        Regex regex = new Regex(@"\W*");

        foreach (string _article in articles) {
            //Убераем из артикула все символы которые не соответствуют алфавитно-цифровому символу
            string article = regex.Replace(_article, "");

            loadText.text = $"{articles.IndexOf(_article) + 1}/{articles.Count}";

            string price = "0";
            string details = "";
            string resultWarehouse = "";
            string resultAvailability = "";
            string resultDeadline = "";
            yield return new WaitForSeconds(Delay);
            string uri = $"http://proparts.kg/?pbrandnumber={article}&pbrandname={pbrandName}&selectLinkName=deadline&selectSortDirection=0#deadline={deadLine},brands={brands}";

            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
            {
                webRequest.SetRequestHeader("Cookie", DataManager.instance.Cookie);

                yield return webRequest.SendWebRequest();
                SetErrorMessage("", Color.black);
                if (webRequest.isNetworkError)
                {
                    SetErrorMessage(webRequest.error, Color.red);
                    Debug.Log("Error: " + webRequest.error);
                    loadPanel.SetActive(false);
                    yield break;
                }
                else
                {
                    try
                    {
                        HtmlDocument res = new HtmlDocument();
                        res.LoadHtml(webRequest.downloadHandler.text);
                        //Таблица
                        var table = res.DocumentNode
                            .Descendants()
                            .FirstOrDefault(n => n.Id == "searchResultsTable")
                            ?.ChildNodes
                            .FirstOrDefault(n => n.Name == "tbody");
                        //Необходимая строка в таблице
                        var tr = table?
                            .ChildNodes
                            .FirstOrDefault(e => e.Name == "tr" 
                            && e.GetAttributeValue("data-current-brand-number", "") == $"{article.ToUpper().Trim()}_{brands}"
                            && e.GetAttributeValue("data-deadline-max", "") != "0");//игнорируем товар на складе
                        //Цена
                        price = tr?
                                .ChildNodes.Where(e => e.Name == "td")
                                .First(e => e.GetAttributeValue("class","").Contains("resultPrice"))
                                .InnerText.Replace("сом", "").Trim();
                        //Наименование склада
                        resultWarehouse = tr?
                            .ChildNodes.Where(e => e.Name == "td")
                            .First(e => e.GetAttributeValue("data-label", "").Trim() == "Склад")
                            .InnerText.Trim();
                        //Наличие
                        resultAvailability = tr?
                            .ChildNodes.Where(e => e.Name == "td")
                            .First(e => e.GetAttributeValue("data-label", "").Trim() == "Нал.")
                            .InnerText.Trim();
                        //Сроки
                        resultDeadline = tr?
                            .ChildNodes.Where(e => e.Name == "td")
                            .First(e => e.GetAttributeValue("data-label", "").Trim() == "Ожид. срок")
                            .InnerText.Trim().Replace("\t", "").Replace("\n"," ");
                        
                        //Если нет строки с заданными параметрами
                        int.TryParse(tr?.GetAttributeValue("data-deadline-max", ""), out var deadLineMax);
                        if (deadLine < deadLineMax/24){
                            //deadLineMax хранится в часах поэтому делим на 24 что бы получить количество дней
                            details = "Не соответствует заданным параметрам";
                        }

                    }
                    catch (Exception e)
                    {
                        SetErrorMessage(e.Message, Color.red);
                        details = errorText.text;
                        Debug.LogError(e.Message);
                    }
                }
                
                result.Add(
                    _article + ";" +
                    resultWarehouse + ";" +
                    resultAvailability + ";" +
                    resultDeadline + ";" +
                    price + ";" +
                    details);
            }
        }
        loadPanel.SetActive(false);
        SaveResult(result);
    }


    /// <summary>
    /// Обрабатывает и сохраняет результат
    /// </summary>
    private void SaveResult(List<string> result)
    {
        string save_path = StandaloneFileBrowser.SaveFilePanel("Сохранить файл", "", FileName, "txt");
        string csv = "";
        foreach (string s in result)
        {
            csv += s + "\n";
        }

        File.WriteAllText(save_path, csv, Encoding.UTF8);
        if (errorText != null)
        {
            SetErrorMessage(save_path, new Color(0f, 0.5f, 0f));
        }
    }

    /// <summary>
    /// Устанавливает информационный текст
    /// </summary>
    /// <param name="message">Сообщение</param>
    /// <param name="color">Цвет текста</param>
    private void SetErrorMessage(string message, Color color)
    {
        errorText.transform.FindChild("Text").GetComponent<Text>().color = color;
        errorText.text = message;
    }
}
