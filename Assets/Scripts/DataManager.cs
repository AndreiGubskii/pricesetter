﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour {
    public static DataManager instance;

    public string Cookie { get; set; }
    public string Login { get; set; }

    private void Awake() {
        instance = this;
        DontDestroyOnLoad(this);
    }
}
